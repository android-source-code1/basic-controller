package com.example.that_thon.helloandroid;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // create instance resource
    EditText et_first_name;
    EditText et_last_name;
    RadioGroup rg_gender;
    RadioButton rdo_gender;


    Button btn_result;
    TextView tv_result;

    AutoCompleteTextView autoCompleteTextView;
    CheckBox pizza,coffe,burger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String title = getResources().getString(R.string.Info);
        setTitle(title);
//        getActionBar().setIcon(R.drawable);

        addAgreeListener();
    }

    public void addAgreeListener(){
        // create resource references
        et_first_name = (EditText)findViewById(R.id.first_name);
        et_first_name.requestFocus();
        et_last_name = (EditText)findViewById(R.id.last_name);
        rg_gender = (RadioGroup)findViewById(R.id.rg_gender);
        btn_result = (Button)findViewById(R.id.btn_agree);
        tv_result = (TextView)findViewById(R.id.result);

        pizza = (CheckBox)findViewById(R.id.check_pizza);
        coffe = (CheckBox)findViewById(R.id.check_coffee);
        burger = (CheckBox)findViewById(R.id.check_Burger);

        autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.act_view);


        // get array from resurces
        final String []countries = getResources().getStringArray(R.array.countries_array);
        // set adapter to auto complete views
        ArrayAdapter<String> arrayAdapterAuto = new ArrayAdapter<>(this,android.R.layout.select_dialog_item,countries);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setAdapter(arrayAdapterAuto);
        autoCompleteTextView.setTextColor(Color.RED);

        btn_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String firstName = et_first_name.getText().toString();
                String lastName = et_last_name.getText().toString();
                rdo_gender = (RadioButton)findViewById(rg_gender.getCheckedRadioButtonId());
                String gender = rdo_gender.getText().toString();
                String country = autoCompleteTextView.getText().toString();

                if(pizza.isChecked()){
                    Toast.makeText(getApplicationContext(),"Pizza checked.",Toast.LENGTH_SHORT).show();
                }
                if(coffe.isChecked()){
                    Toast.makeText(getApplicationContext(),"Coffee checked.",Toast.LENGTH_SHORT).show();
                }
                if(burger.isChecked()){
                    Toast.makeText(getApplicationContext(),"Burger checked.",Toast.LENGTH_SHORT).show();
                }
//                Toast.makeText(getApplicationContext(),country,Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        String str="";
        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.check_pizza:
                str = checked?"Pizza Selected":"Pizza Deselected";
                break;
            case R.id.check_coffee:
                str = checked?"Coffee Selected":"Coffee Deselected";
                break;
            case R.id.check_Burger:
                str = checked?"Burger Selected":"Burger Deselected";
                break;
        }
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
    }
}
